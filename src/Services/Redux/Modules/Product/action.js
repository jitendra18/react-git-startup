import { createAction } from "redux-actions";
import { constants } from "./constants";

export const fetchAllProduct = createAction(constants.FETCH_ALL_PRODUCT);