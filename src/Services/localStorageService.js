
export function _setAccessToken(token) {
    return localStorage.setItem("accessToken", token);
}
export function _getAccessToken() {
    return localStorage.getItem("accessToken");
}

export function _clearToken() {
    localStorage.removeItem("accessToken");
}

export function _clearLocalStorage() {
    localStorage.clear();
}

export function _isLogin() {
    return localStorage.getItem("accessToken");
}

