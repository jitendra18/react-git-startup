import { lazy } from 'react';
// 
const Home = lazy(() => import("../Pages/Home/Home"))
const Register = lazy(() => import("../Pages/Auth/Register"))
const Login = lazy(() => import("../Pages/Auth/Login"))
const Dashboard = lazy(() => import("../Pages/Dashboard/Dashboard"))


export const privateRoutes = [
  {
    path: "/dashboard",
    element: <Dashboard />,
    exect: true,
    restricted: true
  }
]

export const publicRoutes = [
  {
    path: "/login",
    element: <Login />,
    restricted: false
  },
  {
    path: "/register",
    element: <Register />,
    restricted: false
  },
  {
    path: "/",
    element: <Home />,
    exect: true,
    restricted: false
  },
  {
    path: "/home",
    element: <Home />,
    exect: true,
    restricted: false
  },
]