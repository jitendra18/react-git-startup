export const initialState = () => ({
    allProducts: [],
    fetchAllProductsPending: false,
    fetchAllProductsFailure: false,
    fetchAllProductsSuccess: true,
});
