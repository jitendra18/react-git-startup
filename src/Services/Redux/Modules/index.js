import { combineReducers } from "redux";
import { all } from "redux-saga/effects";
// SAGA
import productSaga from "./Product/saga";
// REDUCERS
import productReducer from "./Product/reducer";
 
const appReducer = combineReducers({
    product: productReducer,
});

export const reducers = (state, action) => {
    if (action.type === 'LOGOUT') {
        return appReducer(undefined, action)
    }
    return appReducer(state, action)
}

export const rootSaga = function* () {
    yield all([
        productSaga(),
    ]);
};
