import { useNavigate } from "react-router-dom";
// 
import './style.scss';


export default function Home() {
  const navigate = useNavigate()
    return (
        <>
            <div className="title">Home Page !</div>
            <div className="nav-btn" onClick={() => navigate("/dashboard")}>Click To Dashboard</div>
        </>
    )
}
