
import React from 'react';
import { Navigate, Outlet } from 'react-router-dom';
import { _isLogin } from '../Services/localStorageService';

const PrivateRoute = () => {
    if (!_isLogin()) {
        return <Navigate to={'/login'} replace />;
    }
    return <Outlet />;
};

export default PrivateRoute;