const keyMirror = require("keymirror");

export const constants = keyMirror({
  FETCH_ALL_PRODUCT: undefined,
});
