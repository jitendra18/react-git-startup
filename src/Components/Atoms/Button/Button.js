export default function Button({ text = "Button", className = "", onClick = () => { }, style={} }) {
    return (
        <div className={`customButton ${className}`} style={style} onClick={onClick}>{text}</div>
    )
}
