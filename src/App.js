import { Suspense } from 'react';
import { PersistGate } from 'redux-persist/integration/react';
import { Provider } from 'react-redux';
import { BrowserRouter, Routes, Route } from 'react-router-dom';
//
import "./Assets/Styles/fontStyle.scss";
// 
import { store, persistor } from './Services/Redux/store';
// 
import PrivateRoute from './Routes/PrivateRoute';
import { privateRoutes, publicRoutes } from './Routes/Routes';

function App() {
  return (
    <BrowserRouter>
      <Suspense loading={<div>Loading  ....</div>}>
        <Provider store={store}>
          <PersistGate loading={null} persistor={persistor}>
            <Routes>
              <Route element={<PrivateRoute />}>
                {
                  privateRoutes.map((route, key) => {
                    return <Route key={key} {...route} />
                  })
                }
              </Route>
              {
                publicRoutes.map((route, key) => {
                  return <Route key={key} {...route} />
                })
              }
            </Routes>
          </PersistGate>
        </Provider>
      </Suspense>
    </BrowserRouter>
  );
}

export default App;
