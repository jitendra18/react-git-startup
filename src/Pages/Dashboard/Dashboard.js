import { useNavigate } from "react-router-dom";
// 
import { _setAccessToken } from "../../Services/localStorageService";
import "./style.scss";

export default function Dashboard() {
  const navigate = useNavigate()

  const handleClick = () => {
    _setAccessToken(null)
    navigate("/login",{replace:true})
  }

  return (
    <>
      <div className="title">Dashboard Page !</div>
      <div className="action-wrapper">
        <div className="logout-btn" onClick={handleClick}>Click To Logout</div>
        <div className="nav-btn" onClick={() => navigate("/home")}>Click To Home</div>
      </div>
    </>
  )
}
