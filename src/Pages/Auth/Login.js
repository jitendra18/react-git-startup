import { useNavigate } from "react-router-dom";
// 
import "./style.scss";
import { _setAccessToken } from '../../Services/localStorageService';

export default function Login() {
    let navigate = useNavigate();

    const handleClick = () => {
        _setAccessToken("Token")
        navigate("/dashboard")
    }

    return (
        <>
            <div className="title">Login Page !</div>
            <div className="action-wrapper">
                <div className="login-btn" onClick={handleClick}>Click To Login</div>
                <div className="nav-btn" onClick={() => navigate("/home")}>Click To Home</div>
            </div>
        </>
    )
}
